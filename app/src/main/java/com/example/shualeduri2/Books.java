package com.example.shualeduri2;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Books{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "BOOK_ID")
    private Long bookId;

    @ColumnInfo(name = "BOOK_NAME")
    private String bookname;

    @ColumnInfo(name = "BOOK_AUTHOR")
    private String bookauthor;

    @ColumnInfo(name = "BOOK_PRICE")
    private Float bookprice;

    public Books() {
    }

    public Books (Long bookId, String bookname, String bookauthor, Float bookprice) {
        this.bookId = bookId;
        this.bookname = bookname;
        this.bookauthor = bookauthor;
        this.bookprice = bookprice;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookauthor() {
        return bookauthor;
    }

    public void setBookauthor(String bookauthor) {
        this.bookauthor = bookauthor;
    }

    public Float getBookprice() {
        return bookprice;
    }

    public void setBookprice(Float bookprice) {
        this.bookprice = bookprice;
    }

    @Override
    @NonNull
    public String toString() {
        return "Books{" +
                "bookId=" + bookId +
                ", bookname='" + bookname + '\'' +
                ", bookauthor='" + bookauthor + '\'' +
                ", bookprice=" + bookprice +
                '}';
    }
}
