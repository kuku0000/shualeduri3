package com.example.shualeduri2;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;
@Dao
public interface BookDao {
    @Query("SELECT * FROM BOOKS")
    List<Books> getAllBooks();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Books... Book);

    @Delete
    void delete(Books Book);

    @Query("DELETE FROM BOOKS")
    void deleteAll();

    @Query("SELECT * FROM Books C WHERE C.BOOK_NAME LIKE :bookname AND C.BOOK_PRICE > :bookprice LIMIT 1")
    Books getByNameAndPrice(String bookname, Float bookprice);

}

