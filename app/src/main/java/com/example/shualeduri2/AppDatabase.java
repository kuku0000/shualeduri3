package com.example.shualeduri2;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Books.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    abstract BookDao getBookDao();
}